const path = require("path");
const webpack = require('webpack')
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: {
    index: "./src/scripts/index.js",
    about: "./src/scripts/about.js",
    product: "./src/scripts/product.js"
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].js",
    chunkFilename: '[id].[chunkhash].js',
  },
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: [
          {
            loader: "style-loader",
          },
          {
            loader: "css-loader",

          },
          {
            loader: "postcss-loader", // Run post css actions
            options: {
              plugins: function () {
                return [require("precss"), require("autoprefixer")];
              },
            },
          },
          {
            loader: "sass-loader",
          },
        ],
      },
      {
        test: /\.(png|svg|jpe?g|gif)$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'images/'
          }
        }]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/'
          }
        }
        ],
      },
      {
        test: /\.(html)$/,
        loader: 'html-loader'
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: "./src/pages/index.html",
      filename: "index.html",
      chunks: ['index'],
      minify: {
        collapseWhitespace: true,

        removeComments: true,
        removeEmptyAttributes: true,
        removeRedundantAttributes: true,
        minifyCSS: true,
        minifyURLs: true,
        minifyJS: true,
      },
    }),
    new HtmlWebpackPlugin({
      inject: true,
      template: "./src/pages/about.html",
      filename: "about.html",
      chunks: ['about'],
      minify: {
        collapseWhitespace: true,
        removeComments: true,
        removeEmptyAttributes: true,
        removeRedundantAttributes: true,
        minifyCSS: true,
        minifyURLs: true,
        minifyJS: true,
      },
    }),
    new HtmlWebpackPlugin({
      inject: true,
      template: "./src/pages/product.html",
      filename: "product.html",
      chunks: ['product'],
      minify: {
        collapseWhitespace: true,
        removeComments: true,
        removeEmptyAttributes: true,
        removeRedundantAttributes: true,
        minifyCSS: true,
        minifyURLs: true,
        minifyJS: true,
      },
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    })
  ],
};
