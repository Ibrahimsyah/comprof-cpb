export default [
  {
    id: 0,
    kategori: "Pelumas mesin diesel kapal<br/>&<br/> pembangkit listrik",
    gambar:
      "./images/product/pelumas-roda-gigi-industri.png",
    produk: [
      {
        id: 0,
        nama: "Cladium 120 SAE 30, 40",
        deskripsi:
          "Pelumas untuk mesin diesel kapal, industri (genset) dan lokomotif, dengan BBM MGO dan MDO berkualitas tinggi dengan kadar belerang sampai 1% wt.",
        Kinerja: ["API CF", "TBN 12"],
      },
      {
        id: 1,
        nama: "Cladium 200 SAE 30, 40",
        deskripsi:
          "Pelumas untuk mesin diesel kapal 4 langkah dengan BBM MDO berkualitas menengah dengan kadar belerang sampai 2 .0%wt",
        Kinerja: ["API CF", "TBN 20"],
      },
      {
        id: 2,
        nama: "Cladium 300 SAE 30, 40",
        deskripsi:
          "Pelumas untuk mesin diesel kapal 4 langkah dengan BBM MFO berkualitas baik, berkadar belerang tinggi sampai 3.0% wt, sehingga memerlukan TBN tinggi.",
        Kinerja: ["API CF", "TBN 30"],
      },
      {
        id: 3,
        nama: "Cladium 400 SAE 40",
        deskripsi:
          "Pelumas untuk mesin diesel kapal 4 langkah dengan BBM MFO berkualitas rendah, berkadar belerang sangat tinggi 4.0% wt, sehingga memerlukan TBN sangat tinggi",
        Kinerja: ["API CF", "TBN 40"],
      },
      {
        id: 4,
        nama: "Cladium S 140 SAE 40",
        deskripsi:
          "Pelumas untuk mesin diesel kapal, industri (genset) dan !okomotif, dengan BBM MGO atau MDO dengan kadar be,erang sampai 1.5% wt.",
        Kinerja: ["TBN 14", "API CF", "CATERPILLAR", "MAK", "WARTSILA"],
      },
      {
        id: 5,
        nama: "Cladium S 300 SAE 30, 40",
        deskripsi:
          "Pelumas mesin diesel kapal 4 langkah dengan BBM MFO berkualitas tinggi dan kadar belerangnya tinggi sampai 3.0% wt, dilengkapi dengan additive TBN khusus untuk menghindari penumpukan karbon hasil pembakaran",
        Kinerja: ["TBN 30", "API CF", "CATERPILLAR", "MAK", "WARTSILA"],
      },
      {
        id: 6,
        nama: "Cladium S 400 SAE 30, 40",
        deskripsi:
          "Pelumas mesin diesel kapal 4 langkah dengan BBM MFO berkualitas rendah dan kadar belerangnya tinggi sampai 4%wt, dilengkapi dengan additive TBN khusus untuk menghindari penumpukan karbon hasil pembakaran.",
        Kinerja: ["TBN 40", "API CF", "CATERPILLAR", "MAK", "WARTSILA"],
      },
      {
        id: 7,
        nama: "Cladium S 500 SAE 30, 40",
        deskripsi:
          "Pelumas mesin diesel kapal 4 langkah dengan BBM MFO berkualitas sangat rendah dan kadar belerangnya tinggi sampai 5%wt, dilengkapi dengan additive TBN khusus untuk menghindari penumpukan karbon hasil pembakaran.",
        Kinerja: ["TBN 50", "API CF", "CATERPILLAR", "MAK", "WARTSILA"],
      },
      {
        id: 8,
        nama: "Cladium 50 SAE 30",
        deskripsi:
          "Pelumas mesin umumnya kapal/genset/industry berkecepatan rendah yang digunakan melumasi bagian krukas (poros engkol) dari tipe mesin berkepala piston melintang besar (tipe 2 langkah).",
        Kinerja: ["TBN 5", "API CF"],
      },
      {
        id: 9,
        nama: "Punica 550 SAE 50",
        image:'./images/product/pelumas-mesin-diesel-kapal-pembangkit-listrik.png',
        deskripsi:
          "Dibuat guna memberikan pelumasan silinder yang unggul pada mesin diesel kepala silang (tipe 2 langkah) dengan BBM MFO dengan kadar belerang di bawah 5% wt. Dapat memberikan pelumasan yang optimum sekalipun dipakai untuk mesin-mesin besar dengan efektif rata-rata tertinggi dikelasnya.",
        Kinerja: ["NEW SULZER", "MAN-B&W", "TBN 50"],
      },
      {
        id: 10,
        nama: "Punica 570 SAE 50",
        image:'./images/product/pelumas-mesin-diesel-kapal-pembangkit-listrik.png',
        deskripsi:
          "Dibuat guna memberikan pelumasan silinder yang unggul pada mesin diesel kepala silang (Cross Head Cylinder) dengan BBM HFO dengan kadar belerang di atas 5% wt. Dapat memberikan pelumasan yang optimum sekalipun dipakai untuk mesin-mesin besar dengan efektif rata-rata tertinggi dkelasnya.",
        Kinerja: ["NEW SULZER", "MAN-B7W", "TBN 70"],
      },
    ],
  },
  {
    id: 1,
    kategori: "Pelumas Mesin Diesel",
    gambar: "./images/product/pelumas-mesin-gas.png",
    produk: [
      {
        id: 0,
        nama: "Super Turbo Diesel SAE 15W-40",
        deskripsi:
          "Pelumas berkekentalan ganda dengan masa ganti oli sangat panjang untuk mesin diesel umum atau dilengkapi dengan turbocharger intercooler yang bekerja sangat berat pada kendaraan, truk, bus dan alat berat.",
        Kinerja: [
          "TBN 11",
          "API CH-4/SG",
          "ACEA E7, E5, E3, B3",
          "MB228.3",
          "MAN M 3275",
          "MTU Typ 2",
          "ZF TE-ML 04C ",
        ],
      },
      {
        id: 1,
        nama: "Sigma Turbo Plus SAE 15W-40",
        deskripsi:
          "Pelumas berkekentalan ganda dengan masa ganti oli sangat panjang untuk mesin diesel umum atau dilengkapi dengan EGR Exhaust Gas Recirculation yang bekerja sangat berat pada kendaraan, truk, bus dan alat berat.",
        Kinerja: [
          "TBN 11 ",
          "API CI-4/SL ",
          "ACEA E7, E5, E3, B3",
          "MB228.3 ",
          "MAN M 3275 ",
          "MTU Typ 2  ",
          "ZF TE-ML 04C",
          "RVI RD ",
          "VOLVO VDS 3 ",
        ],
      },
      {
        id: 2,
        nama: "Diesel Super Multigrade SAE 15W-40, 20W-50",
        deskripsi:
          "Pelumas berkekentalan ganda cocok untuk mesin diesel umum atau dilengkapi dengan turbocharger pada kendaraan, truk, bus, alat berat serta mesin pembangkit listrik.",
        Kinerja: [
          "TBN 11 ",
          "API CF-4/SG ",
          "MB 228.1 ",
          "MAN 271",
          "MTU Type 1 ",
          "Volvo VDS ",
          "Mack EO-K/2",
        ],
      },
      {
        id: 3,
        nama: "Diesel SIgma Plus SAE 10W, 30, 40, 50",
        deskripsi:
          "Pelumas berKinerja tinggi untuk mesin diesel berkecepatan tinggi biasa dan dengan turbocharger untuk tugas berat terus menerus, seperti pada truk, bus, alat berat dan pembangkit listrik.",
        Kinerja: [
          "TBN 11 ",
          "SAE 10W TBN 6 ",
          "API CF/SF ",
          "MIL-L-2104 E Ievel ",
          "MB 227.0 quaIity ",
          "CAT TO-4 ",
          "Allison C-4 ",
        ],
      },
      {
        id: 4,
        nama: "Diesel Sigma SAE 10W, 30, 40, 50",
        deskripsi:
          "Pelumas berKinerja tinggi untuk mesin diesel berkecepatan tinggi biasa dan dengan turbocharger untuk tugas berat terus menerus, seperti pada truk, bus, alat berat serta mesin pembangkit",
        Kinerja: [
          "TBN 1 1 ",
          "SAE 10W TBN 6 ",
          "AP1CD/SF ",
          "MIL-L-2104 E level ",
          "CAT TO-2 ",
          "Allison C-3",
        ],
      },
    ],
  },
  {
    id: 2,
    kategori: "Pelumas Mesin Bensin",
    gambar: "./images/product/pelumas-mesin-gas.png",
    produk: [
      {
        id: 0,
        nama: "Super Motoroil SAE 20W-50",
        deskripsi:
          "Pelumas mesin bensin diformulasi dari mineral base oil pilihan mengandung additive: detergent-dispersant, anti oksidasi, anti aus dan anti foam yang memberi proteksi maksimal Pada mesin & mencegah pembentukan sludge.",
        Kinerja: ["API SG/CD ", "CCMC G2,D2 "],
      },
      {
        id: 1,
        nama: "Super SL Motoroil SAE 20W - 50",
        deskripsi:
          "Pelumas mesin bensin diformulasi dari mineral base oll bermutu tinggi dengan addltive pilihan, mempunyai kestabilan kekentalan tinggi, mencegah deposit dan pembentukan pernis/kerak pada piston, tahan terhadap oksIdasi pada temperatur tinggi.",
        Kinerja: ["API SL/CF"],
      },
      {
        id: 2,
        nama: "Formula 3000 SAE 10W-40",
        deskripsi:
          'Pelumas mesin bensin diformulasi dari sintetik base oil dengan additive khusus, detergent-dispersant, anti oksidasi, anti aus dan anti foam mempunyai "shear stability" pelumasan optimal pada temperatur rendah maupun tinggi dengan tingkat penguapan yang rendah. Juga sesuai untuk aplikasi konverter katalitik.',
        Kinerja: ["API SM"],
      },
      {
        id: 3,
        nama: "F1 SMO SAE 15W-40",
        deskripsi:
          "Pelumas mesin multi fungsi untuk mesin bensin maupun diesel dengan kekentalan ganda, sangat cocok untuk aplikasi dimana membutuhkan pelumas tunggal untuk mesin bensin sekaligus diesel untuk kendaraan kecil seperti armada rental, travel dan/atau taksi.",
        Kinerja: [
          "API SL/CF ",
          "ACEA A3,B3 ",
          "MB 299.1 ",
          "MIL-L-46152D ",
          "CCMC G4, Pd2 ",
          "Ford USA-ESE-M2C 153-3 ",
          "GM 6085M ",
          "Rover RES.22.OL.02.6-4 ",
          "VW 501,01 + 505.00 (11/92) ",
        ],
      },
    ],
  },
  {
    id: 3,
    kategori: "Pelumas Mesin Gas",
    gambar: "./images/product/pelumas-mesin-gas.png",
    produk: [
      {
        id: 0,
        nama: "Geum 540 SAE 40",
        deskripsi:
          "Pelumas mesin gas berkualitas tinggi untuk mesIn menggUnakan bahan bakar Natural Gas dan LPG dengan kandungan “ash” 0.35% - 0.5% (low ash).",
        Kinerja: [
          "API CF ",
          "TBN 6.2 ",
          "WAUKESHA ",
          "CATERPILLAR ",
          "WARTSILA ",
          "GE JENBACHER ",
          "DEUTZ MWM ",
          "GUASCCOR",
        ],
      },
      {
        id: 1,
        nama: "Geum 840",
        deskripsi:
          "Pelumas mesin gas berkualitas tinggi untuk mesIn menggUnakan bahan bakar Natural Gas dan LPG dengan kandungan “ash” 0 5% - 1% (medium ash)",
        Kinerja: [
          "API CF ",
          "TBN 9.2 ",
          "WAUKESHA ",
          "CATERPILLAR ",
          "WARTSILA",
          "GE JENBACHER ",
          "DEUTZ MWM ",
          "GUASCCOR",
        ],
      },
    ],
  },
  {
    id: 4,
    kategori: "Pelumas Roda Gigi <br/>&<br/> Transmisi Otomotif",
    gambar:
      "./images/product/pelumas-roda-gigi-dan-transmisi-otomotif.png",
    produk: [
      {
        id: 0,
        nama: "Rotra MP SAE 90, 140, 80W-90, 85W-140",
        deskripsi:
          "Pelumas roda gigi tekanan ekstrim dianjurkan untuk roda gigi hipoid dengan beban tinggi yang terdapat pada rumah gigi dan kemudi kendaraan, truk serta gardan alat berat dan pertanian.",
        Kinerja: [
          "API GL-5, MIL-L-2105D, ",
          "FORD M2C 105A. M2C-A54A, ",
          "MAN 341 Typ Ml, ",
          "CHRYSLER MS-5644, ",
          "FORD SQM-2C-9002N 9008A/9101A, GM MS 9985290, ",
          "MAN 342 Typ N, ",
          "OPEL B 0401010, VW TL 727/726, ",
          "VOLVO 97310/97313/97314, ",
          "ZF TE-ML 01,05A,07A,168.16C,1713,198,21A. ",
          "LIEBHERR ",
        ],
      },
      {
        id: 1,
        nama: "Rotra HY SAE 90, 80W-90, 85W-140",
        deskripsi:
          "Pelumas roda gigi tekanan tinggi dianjurkan untuk roda gigi hipoid dengan beban normal yang terdapat pada rumah glgl dan kemudi kendaraan, truk serta gardan alat berat dan pertanian.",
        Kinerja: [
          "API GL-4 ",
          "MIL-1-2105 level ",
          "MAN 341 Type EI,Z1 ",
          "ZF TE-ML 2A16A.17A.19A ",
          "VW TL 726 level ",
          "LIEBHERR ",
          "MB 235.1 ",
        ],
      },
      {
        id: 2,
        nama: "ROTRA CT SAE 10W, 30, 50",
        deskripsi:
          "Pelumas multiguna untuk alat berat pertambangan dan konstruksi dengan spesifikasi CATERPILLAR TO-4, yang digunakan untuk tenaga hydraulic geseran, transmisi penggerak langsung (maupun transmisi manual yang dulunya TO-2), penggerak akhir, gardan, penggulung (kabel/rantai), plat kopling dan rem tipe basan, beberapa gear dan bantalannya.",
        Kinerja: [
          "CAT TO-4 ",
          "Alllson C-4 ",
          "Kornatsu Micro Clucth ",
          "ZF-TE-ML 01.03 ",
          "EATON FULLER ",
          "API CF/CF-2",
        ],
      },
      {
        id: 3,
        nama: "Rotra JDF PLUS SAE 10W-30 / 80W",
        deskripsi:
          "Pelumas multiguna untuk alat berat konstruksi dan traktor modern dengan spesifikasi UTTO yang digunakan untuk pelumasan terpadu pada gearbox, gardan, kopling dan rem jenis basah, sistem hidrolik, penggerak akhir dan gearbox berdaya lepas.",
        Kinerja: [
          "API GL-4",
          "CAT TO-2",
          "ALLISON C-4",
          "JOHN DEERE JDM J 20C",
          "MASSEY FERGUSON M1135, M1141, M1143, M1145",
          "KUBOTA UDT FLUID",
          "KOMATSU (KES 07.866)",
          "CASE NEW HOLLAND MAT 3525, MS 1207, 1210",
          "FORD ESN-M2C86-B, ESN-M2C86-C, ESN-M2C134-D,",
          "FNHA-2-C-201,",
          "ZF TE-ML 03E, 06K, 17E,",
          "VCE-WB 102 ",
        ],
      },
      {
        id: 4,
        nama: "ATF 2 D DEXRON 11",
        deskripsi:
          "Fluida khusus untuk transmisi otomatis modern dan power steering banyak pemakaian lain yang menghendaki Kinerja pada cuaca dingin, stabil terhadap beban mekanis serta indeks kekentalan tinggi.",
        Kinerja: ["DEXTRON III H "],
      },
      {
        id: 5,
        nama: "ROTRA MULT1 ATF DEXRON 111 H",
        deskripsi:
          "Fluida khusus transmisi otomatis modern dc3n power steering banyak pemakaian yang menghendaki kinela pada cuaca dingin, stabil terhadap beban mekanis serta Indeks kekentalan tinggi.",
        Kinerja: [
          "GM Dexton IIIH ",
          "MERCON V",
          "Allison C-4 ",
          "Chrysler ATF-4 ",
          "JASO 1A ",
          "Toyota T-IV",
          "Honda ATF-ZI (EXCEPT CVTs) ",
          "Mitsubishi Diamond SP-II/III",
          "Nissan Matic D/J/K",
        ],
      },
    ],
  },
  {
    id: 5,
    kategori: "Fluida Hidrolik",
    gambar: "./images/product/fluida-hidrolik.png",
    produk: [
      {
        id: 0,
        nama: "OSO ISO VG 22,32,46,68,100",
        deskripsi:
          "Fluida hidrolik anti aus dibuat untuk semua jenis dan peralatan hidrolik, seperti peralatan angkutan, metalurgl dan alat mesin. Dapat juga digunakan sebagai pelumas tugas berat untuk bantalan, roda gigi reduksi, dsb.",
        Kinerja: [
          "DIN 51524 TEIL 2 HLP ",
          "CETOP RP 91 H HM",
          "AFNOR NF E 48603 HM ",
          "DENISON HF 0 ",
          "ISO L-HM ",
          "ISO 11158 Type HM ",
          "VDMA 24318 ",
          "BS 4231 HSD ",
          "AISE 127 ",
          "EATON VICKERS I-286-S3 ",
          "SAUER-DANFOSS 520L0463 Rev. F",
        ],
      },
      {
        id: 1,
        nama: "Arnica ISO VG 22,32,46,68,100",
        deskripsi:
          "Fluida hidrolik dengan indeks kekentalan dan mampu tuang sangat tinggi, digunakan untuk melumasi sistem hidrolik dengan kontrol servo elektro hidrolik, klep hidrolik dengan rentang suhu kerja yang luas serta operasi yang akurat.",
        Kinerja: [
          "DIN 51524 TEIL 3 HVLPI",
          "CETOP RP 91 H HV ",
          "AFNOR NF E 48603 HV ",
          "DENISON HF 0",
          "VICKERS M-2950 ",
          "CICINNATI P-68, P-69 AND P-70 ",
          "ISO L-HV ",
          "BS 4231 HSE ",
        ],
      },
      {
        id: 2,
        nama: "Arnica / I ISO VG 46,68",
        deskripsi:
          "Fluida hidrolik dengan indeks kekentalan dan mampu tuang sangat tinggi, digunakan untuk melumasi sistem hidrolik dengan kontrol servo elektro hidrolik, klep hidrolik, yang mensyaratkan tanpa abu (ashless), dengan rentang suhu kerja yang luas serta operasi yang akurat.",
        Kinerja: [
          "DIN 51524 TEIL 3 ",
          "HVLP CETOP RP 91 H HV",
          "AFNOR NF E 48603 HV ",
          "EATON VICKERS I-286-S3 ",
          "EATON VICKERS M-2950 ",
          "ISO 1-HV ",
          "ISO 11158 ",
          "BS 4231 HSE ",
          "AISE 127 ",
          "ATOS Tab. P 002-0/1 ",
          "LINDE ",
          "DENISON HF-0 ",
          "REXROTH RE 90220-1/11.02 ",
          "SAUER-DANFOSS 520L0464",
        ],
      },
    ],
  },
  {
    id: 6,
    kategori: "Pelumas Sirkulasi R&O",
    gambar: "./images/product/pelumas-sirkulasi-r-o.png",
    produk: [
      {
        id: 0,
        nama: "Acer ISO VG 10,22,32,68,100,150,220,320",
        deskripsi:
          "Pelumas R&O digunakan untuk sistem sirkulasi, hidrolik, kompresor, blower turbo, pompa, bantalan, pelumas umum yang tidak memerlukan pelumas anti aus.",
        Kinerja: [
          "DIN 51524 TEIL 1 HL ",
          "AFNOR NF E 48603 HL ",
          "DENISON HF 1A ",
          "DIN 51056 VBL ",
          "ISO L-DAB ",
          "ISO L-HL ",
          "BS 4231 HSC ",
        ],
      },
      {
        id: 1,
        nama: "Acer MPK ISO VG 220",
        deskripsi:
          "Pelumas R&O yang dibuat khusus untuk melumasi bearing dan roda gigi pada mesin-mesin papermill.",
        Kinerja: [
          'BELOIT (Chart 1-21-127 19821) OIL "C',
          'CARCANO (A4-415-5 I 1981) OIL "G',
        ],
      },
      {
        id: 2,
        nama: "Radula 320 ISO VG 320",
        deskripsi:
          "Pelumas R&O digunakan untuk sistem sirkulasi, hidrolik, kompresor, blower turbo, pompa, bantalan, pelumas umum yang tidak memerlukan pelumas anti aus.",
        Kinerja: ["DIN 517 T.1 C "],
      },
    ],
  },
  {
    id: 7,
    kategori: "Pelumas Roda Gigi Industri",
    gambar: "./images/product/pelumas-roda-gigi-industri.png",
    produk: [
      {
        id: 0,
        nama: "Blasia ISO VG 68, 100, 150, 220, 320, 460, 680",
        deskripsi:
          "Pelumas roda gigi (Extreme Pressure), dianjurkan untuk pelumasan celup atau sirkulasi dari semua jenis roda gigi tertutup dengan beban berat, kecepatan tinggi, kecepatan luncur tinggi pada suhu ambien dan kerja tinggi. Dapat juga digunakan untuk melumasi komponen-komponen lain seperti kopling, sekrup transmisi dan bantalan luncur kecepatan rendah.",
        Kinerja: [
          "ISO L-CKD",
          "U.S STEEL 224 ",
          "CINCINNATI MILACRON ",
          "ANSI/AGMA 9005 D94 ",
          "DIN 51517 TEIL. 3 CLP ",
          "DAVID BROWN S1.53.101 ",
          "ASLE EP ",
          "ISO 12925-1 ",
        ],
      },
      {
        id: 1,
        nama: "Blasia SX ISO VG 320",
        deskripsi:
          "Pelumas roda gigi sintetik (Polyalphaolefin) untuk pelumasan celup atau sirkulasi dari semua jenis roda gigi yang beroperasi pada suhu tinggi, untuk suhu 1 20°C - 200° C. Sangat cocok untuk industri gelas, kaca, baja, keramik dan kertas.",
        Kinerja: [
          "ISO 6743-6/CKT ",
          "ANSI/AGMA D9005 D94 (AGMA No.3S 5S, 6S) ",
          "DIN 51517 T.3/CLP 100, 220, 320 ",
        ],
      },
      {
        id: 2,
        nama: "Blasia S ISO VG 320",
        deskripsi:
          "Pelumas roda gigi sintetik (Polyglicol) untuk pelumasan celup atau sirkulasi dari semua jenis roda gigi yang beroperasi pada suhu tinggi, untuk suhu 1 20°C - 200°C. Sangat cocok untuk industri gelas, kaca, baja, keramik dan kertas. Biasanya digunakan pada jenis roda gigi cacing (worm gear).",
        Kinerja: [
          "ISO -L-CKD ",
          "DIN 51502 PGLP ",
          "ANSI/AGMA D9005-D94 (AGMA No. 4S, 5S, 6S)",
        ],
      },
      {
        id: 3,
        nama: "Blasia FMP ISO VG 220, 320",
        deskripsi:
          "Pelumas roda gigi khusus (Extreme Pressure), dianjurkan untuk pelumasan celup atau sirkulasi dari semua jenis roda gigi tertutup dengan beban berat, kecepatan tinggi, kecepatan luncur tinggi pada suhu ambien dan kerja tinggi. Dan dapat mencegah terjadinya micropitting pada roda gigi. Dapat juga digunakan untuk melumasi komponen-komponen lain seperti kopling, sekrup transmisi dan bantalan luncur kecepatan rendah.",
        Kinerja: [
          "ISO-L-CKD ",
          "ISO 12925-1 - CKD ",
          "ANSI/AGMA 9005-D94 (AGMA 3EP, 4EP, 5fP, 6EP. 7EP) ",
          " ASLE EP ",
          " CINCINNATI MILACRON (P-77 IS0150, P-74 IS0220 P-35 ISO460) ",
          " DAVID BROWN S1.53.101 LEVEL ",
          " DIN 51517 teil 3-CLP ",
          " FLENDER BA 7300 Table A ",
          " MUI.LER WEINGARTEN DT 55 005 type CLP ",
          " U.S. STEEL 224 ",
        ],
      },
      {
        id: 4,
        nama: "Arum HT ISO 220",
        deskripsi:
          "Pelumas berbahan dasar sintetik ester yang sangat cocok untuk Pelumasan pada rantai, roda gigi dan bantalan. Dengan index kekentalan yang tinggi membuat pelumas ini mampu beroperasi pada temperatur tinggi, serta dengan aditif yang berkualitas dan base esternya mampu menahan keausan dan oksidasi.",
        Kinerja: [],
      },
    ],
  },
  {
    id: 8,
    kategori: "Pelumas Transfer Panas",
    gambar: "./images/product/pelumas-transfer-panas.png",
    produk: [
      {
        id: 0,
        nama: "Therrn 011 Grade 2. 3, 5",
        deskripsi:
          "Pelumas yang stabil terhadap oksidasi dan panas, untuk mengisi unit transfer panas dengan suhu bak 295°C",
        Kinerja: [],
      },
      {
        id: 1,
        nama: "THERM OIL XT Grade 3 & 5",
        deskripsi:
          "Pelumas yang stabil terhadap oksidasi dan panas, untuk mengisi unit transfer panas dengan suhu bak 325°C",
        Kinerja: [],
      },
    ],
  },
  {
    id: 9,
    kategori: "Pelumas Slideway",
    gambar: "./images/product/pelumas-slideway.png",
    produk: [
      {
        id: 0,
        nama: "Exidia HG ISO VG 68 & 220",
        deskripsi:
          "Pelumas dengan menggunakan bahan dasar parafin yang mempunyai sifat tahan karat, sangat cocok untuk pelumasan pada bagian sliding pada mesin grinding.",
        Kinerja: [
          "CINCINNATI P47 (lSO VG 68), P50 (ISO VG 220) ",
          "SO-L-HG (ISO VG 68) ",
          "ISO-L-G (ISO VG 220) ",
          "ISO-L-CKE (ISO VG 220) ",
          "STANIMUC G ",
          "DIN 51502 CGLP ",
        ],
      },
    ],
  },
  {
    id: 10,
    kategori: "Pelumas Kompresor",
    gambar: "./images/product/pelumas-kompresor.png",
    produk: [
      {
        id: 0,
        nama: "Dicrea ISO VG 32,46,68,100",
        deskripsi:
          "Dirancang untuk pelumasan kompresor udara rotaris (kipas dan sekrup) dan piston,termasuk yang bekerja dengan suhu tekan tinggi (200°C atau lebih)",
        Kinerja: [
          "ISO-L-DAA ",
          "ISO-L -DAG ",
          "DIN 51506 VDL ",
          "ATLAS COPCO ",
          "CECCATO ",
          "BOGE ",
          "FINI ",
          "INDOERSOLL RAND ",
          "HATLAPA ",
          "JML ",
          "KAESER ",
          "MATTEI ",
          "NUOVO PIGNONE",
          "PNEUMOFORE ",
          "SAUER ",
        ],
      },
      {
        id: 1,
        nama: "Dicrea SX ISO VG 32, 46, 68",
        deskripsi:
          "Pelumas sintetik (polyalphaolefin) dirancang untuk pelumasan kompresor udara rotaris (kipas dan sekrup) dan piston,termasuk yang bekerja dengan suhu tekan tinggi (200°C atau lebih).",
        Kinerja: ["IS0-L-DAB ", "ISO-L-DAH ", "DIN 51506 VOL"],
      },
      {
        id: 2,
        nama: "Betula S ISO VG 68",
        deskripsi:
          "Pelumas sintetik yang memiliki titik tuang sangat rendah dan dirancang untuk pelumasan kompresor refrigerator yang bekerja pada suhu rendah, Dapat juga digunakan fluida refrigerator seperti amonia,freon,d11.",
        Kinerja: [
          "DIN 51503 KC ",
          "DIN 51503 KA ",
          "MM-O-2008 type II (NATO O-283) ",
          "MM-O-2008 type IV (NATO O-290) ",
          "VV-L-825b type II (NATO O-283)  ",
          "VV-L-825B type IV (NATO O-290) ",
          "IRE ",
          "SABROE ",
          "ZANUSSI ELECTROLUX",
        ],
      },
    ],
  },
  {
    id: 11,
    kategori: "Pelumas Turbin",
    gambar: "./images/product/pelumas-turbin.png",
    produk: [
      {
        id: 0,
        nama: "OTE ISO VG 32, 46, 68",
        deskripsi:
          "Pelumas berindeks kekentalan tinggi digunakan untuk melumasi semua bagian (bantalan, sistem kendali,dsb) dari berbagai jenis turbin : uap,air dan gas. Dapat juga digunakan untuk pelumas kipas turbo, mesin hidrolik dan kompresor udara.",
        Kinerja: [
          "ABB HTGD 90117",
          "AEG KANIS 14-7-1970 (DIN 51515) ",
          "AEG PV 198851 ",
          "BS 489 ",
          "CEI 10-8 SEPTEMBER 1994-2367 ",
          "CINCINNATI P-38 (VG 32), P55 (VG 46), P54 (VG 68) ",
          "DIN 51515 teil 1 ",
          "ESCHER WYSS 2050995 F ",
          "GE GEH-709 V, GEI-41003 G, GEK 28143",
          "IEC 962-1988",
          "ISO-L-TSA ",
          "KWU TLV 901301 ",
          "NATO 0-240 (MM-0-2001) ",
          "NF 384 AFT 141 ",
          "SIEMENS KR 900/01-1 (DIN 51515) ",
          "UNI lSO 9086 ",
        ],
      },
    ],
  },
  {
    id: 12,
    kategori: "Gemuk",
    gambar: "./images/product/gemuk.png",
    produk: [
      {
        id: 0,
        nama: "Grease MU NLGI 0, 1, 2, 3",
        deskripsi:
          "Gemuk serbaguna berserat sedang, dasar Litium berwarna coklat kekunIngan cocok untuk bantalan luncur, bantalan bola dan gelinding, engsel sambungan, kopling dan bagian-bagian lain dari permesinan Industri, konstruksi, pertanian, dsb.",
        Kinerja: [],
      },
      {
        id: 1,
        nama: "Grease MU EP NLGI 0, 1, 2, 3",
        deskripsi:
          "Gemuk EP dasar Lithium, berwujud lembut, sedikit berserat, berwarna coklat kekuningan untuk pelumasan bantalan luncur,bantalan bola, dan gelinding berbeban berat, dsb, yang bekerja pada suhu tinggi dan kondisi sulit.",
        Kinerja: [],
      },
      {
        id: 2,
        nama: "Grease SM NLGI 2",
        deskripsi:
          "Gemuk litium lembut, berwarna abu-abu kehitaman,mengandung molibdenum disulfida, cocok untuk pelumasan bantalan dengan beban mekanis dan tekan panas sangat tinggi.",
        Kinerja: [],
      },
      {
        id: 3,
        nama: "Grease AC NLGI 2",
        deskripsi:
          "Gemuk EP (Extreme Pressure) dasar alumunium kompleks mempunyai ketahanan terhadap air untuk pelumasan bantalan yang beroperasi pada suhu tinggi. Dapat dipompa sehingga dapat diaplikasikan pada sistem centralized.",
        Kinerja: [],
      },
      {
        id: 4,
        nama: "SP Grease LC NLGI 2",
        deskripsi:
          "Gemuk EP (Extreme Pressure) dasar Litium kompleks, berwujud lembut, sedikit berserat, berwarna coklat kekuningan untuk pelumasan bantalan luncur, bantalan bola, dan gelinding berbeban berat,dsb, yang bekerja pada suhu tinggi, Dikembangkan untuk memenuhi kebutuhan aplikasi pada industri baja.",
        Kinerja: [],
      },
      {
        id: 5,
        nama: "Sagus 60 NLGI 0",
        deskripsi:
          "Gemuk berbahan dasar grafit berwarna hitam pekat untuk pelumasan pada roda gigi terbuka, rantai, bantalan,kawat seling (wire ropes) yang bekerja pada suhu tinggi.",
        Kinerja: [],
      },
      {
        id: 6,
        nama: "Grease 33 FD NLGI 3",
        deskripsi:
          "Gemuk tak leleh dibuat dari pengental khusus, berwujud lembut. berwarna coklat muda untuk pelumasan pada pusat bantalan roda yang dilengkapi dengan rem cakram, sangat cocok untuk aplikasi pada kecepatan dan suhu tinggi",
        Kinerja: [],
      },
    ],
  },
];
