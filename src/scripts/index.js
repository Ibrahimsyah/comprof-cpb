import "regenerator-runtime";
import "bootstrap/js/dist/collapse";
import "bootstrap/js/dist/scrollspy";
import "bootstrap/js/dist/carousel";
import "bootstrap/js/dist/dropdown";
import "../styles/index.scss";
import WOW from "wow.js";

const main = () => {
    new WOW().init();
    $('.nav-link').click(function () {
        var href = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(href).offset().top - $("nav").outerHeight()
        }, 500);
    })
};

$(document).ready(main)
