import "regenerator-runtime";
import "bootstrap/js/dist/collapse";
import "bootstrap/js/dist/scrollspy";
import "bootstrap/js/dist/carousel";
import "bootstrap/js/dist/dropdown";
import "../styles/product.scss";
import product from "../data/products";

const main = () => {
  $(".dropdown-item").click(function () {
    const idx = $(this).attr("href").replace("#", "");
    renderKategori(idx);
  });

  $("main").css("margin-top", $("nav").outerHeight());
  $(".nav-link").click(function () {
    var href = $(this).attr("href");
    if (href.length > 1) {
      $("html, body").animate(
        {
          scrollTop: $(href).offset().top - $("nav").outerHeight(),
        },
        500
      );
    }
  });

  const url = window.location.href;
  const hash = url.substring(url.indexOf("#") + 1);
  if (hash) {
    if (product[hash]) renderKategori(hash);
    else renderKategori(0);
  }
};

const renderKategori = (index) => {
  const produk = product[index];
  let currentIndex = 0
  if (produk) {
    let productList = ``;
    produk.produk.forEach((p) => {
      productList += `
        <div class="product-item" key="${p.id}">
        ${p.nama}
        </div>
        `;
    });
    $(".product-container").html(productList);
    $(".kategori-produk").html(produk.kategori);
    $($(".product-item")[0]).addClass("active");
    renderProduct(index, 0);

    $(".product-item").click(function () {
      $(".product-item.active").removeClass("active");
      $(this).addClass("active");
      const idx = $(this).attr("key");
      if (idx === currentIndex) return
      currentIndex = idx
      renderProduct(index, idx);
    });
  } else {
    alert("produk tidak ditemukan!");
  }
};

const renderProduct = (kategoriIdx, productIdx) => {
  const produk = product[kategoriIdx].produk[productIdx];
  if (produk) {
    let html = ``;
    if (produk.Kinerja.length) {
      let kinerjaList = "";
      produk.Kinerja.forEach((kinerja) => {
        kinerjaList += `
            <li>${kinerja}</li>
            `;
      });
      html += `<div class="fadeIn">
          <div class="text-center">
            <strong>Kinerja</strong>
            <ul class="kinerja-list">
              ${kinerjaList}
            </ul>
          </div>
          <br />`;
    }
    html += `<div class="text-center">
    <strong>Deskripsi</strong>
    <p class="deskripsi-produk">${produk.deskripsi}</p>
  </div>
</div>
  `;
    $(".desc-container").html(html);
    $(".image-container").html(`
        <img
            src="${produk.image || product[kategoriIdx].gambar}"
            alt="Gambar ${produk.nama}"
            class="product-image"
        />
        `);
  } else {
    $(".desc-container").html("Belum ada Data");
  }
};
$(document).ready(main);
